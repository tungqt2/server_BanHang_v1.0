const KhachHang = require("../api/KhachHang")
const NganhHang = require("../api/NganhHang")
const TaiKhoan = require("../api/TaiKhoan")
const Token = require("../api/Token")

function Controller(app){
    Token(app) 
    TaiKhoan(app)  
    NganhHang(app)
    KhachHang(app)
}

module.exports = {
    Controller
}