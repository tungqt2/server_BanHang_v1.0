
const express = require('express');
const https = require('https');
const fs = require("fs")
const cors = require('cors');
var bodyParser = require('body-parser')
// Create app
const app = express();
app.use(bodyParser.urlencoded({ extended: false }))
// Create Doc-API
var  swaggerJsdoc = require("swagger-jsdoc")
var  swaggerUi = require("swagger-ui-express")

app.use(cors());
app.use(express.json());


const document = require('./document');
const controller = require('./controller')
//#region SERVER


controller.Controller(app)


//#endregion SERVER












const options = {
	definition: document,
	apis: ["./routes/books.js"],
  };
  
  const specs = swaggerJsdoc(options);
  app.use(
	"/doc-api",
	swaggerUi.serve,
	swaggerUi.setup(specs)
  );


// Setting HTTPS
const optionsHTTPS = {
	key: fs.readFileSync('key.pem'),
	cert: fs.readFileSync('cert.pem')
};

app.listen(3007)
// https.createServer(optionsHTTPS,app).listen(3007)
console.log("Start port 3007")
// 
